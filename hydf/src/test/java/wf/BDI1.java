package wf;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import operation.ReadObject;
import operation.UIOperation;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.common.io.Files;

import objects.*;
import excelExportAndFileIO.ReadGuru99ExcelFile;

public class BDI1 {
	
	
	WebDriver webdriver = null;
    @Test(dataProvider="hybridData")
	public void testLogin(String testcaseName,String keyword,String objectName,String objectType,String value) throws Exception {
    	
   	 try {
    	
		// TODO Auto-generated method stub
    	 System.setProperty("webdriver.chrome.driver", "Z:\\chromedriver.exe");	
    	
    	if(testcaseName!=null&&testcaseName.length()!=0){
    	///webdriver=new FirefoxDriver();
    		webdriver=new ChromeDriver();
    	
    	}
    	webdriver.manage().window().maximize();
    	
        ReadObject object = new ReadObject();
        Properties allObjects =  object.getObjectRepository();
        UIOperation operation = new UIOperation(webdriver);
      	//Call perform function to perform operation on UI
    			operation.perform(allObjects, keyword, objectName,
    				objectType, value);
    			
    			
    			
    			
    			
    			
	 }catch (Exception e)
 {System.out.println(e.getMessage()+"DDDDDDDDDDDDDDDDDDDDD");
// TODO: handle exception

 Logger ll =Logger.getLogger("error ");
 ll.trace(e.getMessage());
 Thread.sleep(4000);
	
	System.out.println("screnshot,,,,,,,,,,,,,,,,,,,");
	File scrFile = ((TakesScreenshot) webdriver).getScreenshotAs(OutputType.FILE);
	DateFormat df = new SimpleDateFormat("ddMMyy HHmmss");
       Date dateobj = new Date();
       String sx= df.format(dateobj);
      // System.out.println(df.format(dateobj));
	Files.copy(scrFile,	new File(System.getProperty("user.dir") + "/screenshot/" +sx+ ".png"));
	webdriver.quit();
 
 
 
}
    	    
	}


   
    
    @DataProvider(name="hybridData")
	public Object[][] getDataFromDataprovider() throws IOException{//
    	
    	
    	System.out.println(this.getClass().getSimpleName()); 
    	
    	String dd=this.getClass().getSimpleName();
    	System.out.println(dd+"ddddddddddddddddddddddd");
    	Object[][] object = null; 
    	ReadGuru99ExcelFile file = new ReadGuru99ExcelFile(); 
         //Read keyword sheet
    	// Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir")+"\\",excelfilename , excelsheetname);
    Sheet guru99Sheet = file.readExcel(System.getProperty("user.dir")+"\\","TestCase3.xlsx" ,dd);
       //Find number of rows in excel file
     	int rowCount = guru99Sheet.getLastRowNum()-guru99Sheet.getFirstRowNum();
     	object = new Object[rowCount][5];
     	for (int i = 0; i < rowCount; i++) {
    		//Loop over all the rows
    		Row row = guru99Sheet.getRow(i+1);
    		//Create a loop to print cell values in a row
    		for (int j = 0; j < row.getLastCellNum(); j++) {
    			//Print excel data in console
    			object[i][j] = row.getCell(j).toString();
    		}
         
    	}
     	System.out.println("");
     	  return object;	 
	}
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
