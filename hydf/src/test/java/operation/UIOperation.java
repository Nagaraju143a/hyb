package operation;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.common.io.Files;

public class UIOperation {

	WebDriver driver;
	public UIOperation(WebDriver driver){
		this.driver = driver;
	}
	public void perform(Properties p,String operation,String objectName,String objectType,String value) throws Exception{
		System.out.println("ddddddddddddddddd");
		switch (operation.toUpperCase()) {
		case "CL":
			//Perform click
	
			driver.findElement(By.xpath(objectName)).click();
			
			break;
		case "ST":
			//Set text on control
			System.out.println(objectName+value);
			driver.findElement(By.xpath(objectName)).sendKeys(value);
				
			break;
		case "URL":
			//Get url of application
			driver.get(value);
			break;
		case "CLICK":
			//Perform click
	
			driver.findElement(this.getObject(p,objectName,objectType)).click();
			Logger l =Logger.getLogger("clicked");
			 l.trace(objectName);
		
			break;
		case "SETTEXT":
			//Set text on control
			driver.findElement(this.getObject(p,objectName,objectType)).sendKeys(value);
			 Logger ll =Logger.getLogger("sendkeys");
			 ll.trace(value);			
			break;
		case "CLEAR":
			//Set text on control
			driver.findElement(this.getObject(p,objectName,objectType)).clear();
			 Logger llc =Logger.getLogger("clear");
			 llc.trace(value);			
			break;
		case "JSDOWNELE":	
		
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(this.getObject(p,objectName,objectType)));
			
			break;
			
		case "SCREEN":
			//Set text on control
			System.out.println("screnshot");
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			DateFormat df = new SimpleDateFormat("ddMMyy HHmmss");
		       Date dateobj = new Date();
		       String sx= df.format(dateobj);
		      // System.out.println(df.format(dateobj));
			Files.copy(scrFile,	new File(System.getProperty("user.dir") + "/screenshot/" +sx+ ".png"));

			
			break;
			
		case "ROBOTPGDOWN":	
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_END);
			robot.keyRelease(KeyEvent.VK_END);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			
			break;
			
			
		case "DRAGDROP":	
			Actions abc= new Actions(driver);
			abc.dragAndDrop(driver.findElement(By.xpath(p.getProperty(objectName))),driver.findElement(By.xpath(p.getProperty(objectType)))).build().perform();
			
			break;
			
		case "DROPDOWN":
			
			Select drop1 = new Select(driver.findElement(this.getObject(p,objectName,objectType)));

			drop1.selectByVisibleText(value);
			break;		
		case "GOTOURL":
			//Get url of application
			driver.get(p.getProperty(value));
			break;
		case "GETTEXT":
			//Get text of an element
			driver.findElement(this.getObject(p,objectName,objectType)).getText();
			break;
		case "WAIT":
			//Get text of an element			
			Thread.sleep(4000);
			break;
			
			
			
		case "URLGET":
			//Get text of an element			
		
			System.out.println(	driver.getCurrentUrl());
			break;
		case "WAITS":
			String wai=p.getProperty(objectName);
				WebDriverWait wait = new WebDriverWait(driver, 60);
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(wai)));
				System.out.println("............. wait for ............");
			
			break;
			
		case "ASSERTS":
			Thread.sleep(2000);
			
			System.out.println("testing ");
			String sk=p.getProperty(objectName);
		
			
		
			
			if(driver.findElement(By.xpath(sk)).getText().equals(value)){
				System.out.println("assert pass");
				Logger al =Logger.getLogger("assert pass");
				 al.trace(objectName);
			}else{
				System.out.println("assert failed");
				Logger al =Logger.getLogger("assert failed");
				 al.trace(objectName);
				
			}
			
			 
			break;	
	
			
			
		case "ISDISPLAYED":	
			
			
			Thread.sleep(2000);
			System.out.println("testing ");
			String sdx=p.getProperty(objectName);
		
			
			
			 if(driver.findElement(By.xpath(sdx)).isDisplayed()){	 Thread.sleep(3000);
				
				System.out.println("disply ");}
			 else{
				 Thread.sleep(2000);
				 System.out.println("not disply");
				
				
					Assert.assertTrue(false);
				}
			
		break;	
			
			
			
		case "ISDISPLAY":	
			
	
			Thread.sleep(2000);
			System.out.println("testing ");
			String s=p.getProperty(objectName);
			 String s2=p.getProperty(objectType);
			System.out.println(s+s2);
			//WebElement we = ;
			
			
			 if(driver.findElement(By.xpath(s)).isDisplayed()){	 Thread.sleep(3000);
				
				System.out.println("clicked ");}
			 else{
				 Thread.sleep(2000);
				 System.out.println("matster clicked then natureofbd ");
				
				 Thread.sleep(2000);
				 
					
					driver.findElement(By.xpath(s2)).click();Thread.sleep(2000);
					
				}
			
		break;
		default:
			break;
		}
	}
	
	/**
	 * Find element BY using object type and value
	 * @param p
	 * @param objectName
	 * @param objectType
	 * @return
	 * @throws Exception
	 */
	private By getObject(Properties p,String objectName,String objectType) throws Exception{
		//Find by xpath
		if(objectType.equalsIgnoreCase("XPATH")){
			
			return By.xpath(p.getProperty(objectName));
		}
		//find by class
		else if(objectType.equalsIgnoreCase("CLASSNAME")){
			
			return By.className(p.getProperty(objectName));
			
		}
		//find by name
		else if(objectType.equalsIgnoreCase("NAME")){
			
			return By.name(p.getProperty(objectName));
			
		}	else if(objectType.equalsIgnoreCase("ID")){
			
			return By.id(p.getProperty(objectName));
			
		}
		//Find by css
		else if(objectType.equalsIgnoreCase("CSS")){
			
			return By.cssSelector(p.getProperty(objectName));
			
		}
		//find by link
		else if(objectType.equalsIgnoreCase("LINK")){
			
			return By.linkText(p.getProperty(objectName));
			
		}
		//find by partial link
		else if(objectType.equalsIgnoreCase("PARTIALLINK")){
			
			return By.partialLinkText(p.getProperty(objectName));
			
		}else
		{
			throw new Exception("Wrong object type");
		}
	}
}
